#!/bin/python
import os
import tempfile
import psutil
import requests
from flask import Flask, jsonify, request
from wtforms import Form, validators, StringField
from app import read_number_plates

app = Flask(__name__)

def download_image(url):
    _, temp_file = tempfile.mkstemp(suffix='.jpg')
    with open(temp_file, 'wb') as f:
        response = requests.get(url)
        f.write(response.content)
    return temp_file


class ReadForm(Form):
    url = StringField('URL to image', [validators.URL()])


@app.route('/read')
def read():
    form = ReadForm(data=request.json)

    if form.validate():
        url = form.url.data
        try:
            temp_file_path = download_image(url)
        except Exception as e:
            return jsonify({
                'success': False,
                'error': str(e)
            })
        try:
            result = read_number_plates(temp_file_path)
        except Exception as e:
            os.remove(temp_file_path)
            return jsonify({
                'success': False,
                'error': str(e)
            })

        os.remove(temp_file_path)

        return jsonify({
            'success': True,
            'result': result,
        })

    return jsonify({
        'success': False,
        'errors': form.errors
    })


@app.route('/status')
def status():
    return jsonify({'success': True, 'result': 'OK'})


@app.route('/disk_space_health_check')
def disk_space_health_check():
    free_space_gb = psutil.disk_usage('/').free / (2**30)
    if free_space_gb < 1:
        response = jsonify({'success': False, 'error': 'Low disk space'})
        response.status_code = 503
        return response
    return jsonify({'success': True, 'result': 'OK'})


@app.route('/memory_usage_health_check')
def memory_usage_health_check():
    memory_usage_percent = psutil.virtual_memory().percent
    if memory_usage_percent > 80:
        response = jsonify({'success': False, 'error': 'High memory usage'})
        response.status_code = 503
        return response
    return jsonify({'success': True, 'result': 'OK'})


@app.route('/cpu_load_health_check')
def cpu_load_health_check():
    cpu_load_percent = psutil.cpu_percent(interval=1)
    if cpu_load_percent > 80:
        response = jsonify({'success': False, 'error': 'High CPU load'})
        response.status_code = 503
        return response
    return jsonify({'success': True, 'result': 'OK'})
