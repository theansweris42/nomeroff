from nomeroff_net import pipeline
from nomeroff_net.tools import unzip

number_plate_detection_and_reading = pipeline("number_plate_detection_and_reading", image_loader="opencv")


def transliterate_to_cyrillic(text):
    translit_dict = {
        'A': 'А',
        'B': 'В',
        'C': 'С',
        'E': 'Е',
        'H': 'Н',
        'K': 'К',
        'M': 'М',
        'O': 'О',
        'P': 'Р',
        'T': 'Т',
        'X': 'Х',
        'Y': 'У'
    }

    cyrillic_text = ''
    for char in text:
        cyrillic_text += translit_dict.get(char, char)

    return cyrillic_text


def read_number_plates(path):
    """
    This function recognizes license plates in a photo and returns them as a dictionary,
    where the key is the license plate and the value is an array of points (coordinates) of the license plate
    """
    global number_plate_detection_and_reading
    result = number_plate_detection_and_reading([path])

    (images, images_bboxs, images_points, images_zones, region_ids,
     region_names, count_lines, confidences, texts) = unzip(result)

    final_results = {}
    for plates, plate_points in zip(texts, images_points):
        for single_plate, single_point in zip(plates, plate_points):
            translated_plate = transliterate_to_cyrillic(single_plate)
            final_results[translated_plate] = single_point

    return final_results
