FROM python:3.9-slim
ENV DEBIAN_FRONTEND noninteractive
ARG USE_NNPACK=0
ENV TZ=Europe/Saratov
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update

RUN apt-get install -y wget
RUN wget https://netix.dl.sourceforge.net/project/libjpeg-turbo/2.0.5/libjpeg-turbo-official_2.0.5_amd64.deb
RUN dpkg -i libjpeg-turbo-official_2.0.5_amd64.deb
RUN rm libjpeg-turbo-official_2.0.5_amd64.deb

RUN apt-get install -y build-essential
RUN apt-get install -y manpages-dev

# For opencv
RUN apt-get install -y libglib2.0

# For pip modules
RUN apt-get install -y git
RUN apt-get install -y libgl1-mesa-glx

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install setuptools
RUN python3 -m pip install "PyYAML>=5.3"
RUN python3 -m pip install scikit_image
RUN python3 -m pip install Cython
RUN python3 -m pip install matplotlib
RUN python3 -m pip install seaborn
RUN python3 -m pip install opencv_python
RUN python3 -m pip install "numpy==1.23.5"
RUN python3 -m pip install "imgaug>=0.4.0"
RUN python3 -m pip install pillow
RUN python3 -m pip install scipy
RUN python3 -m pip install gevent
RUN python3 -m pip install asyncio
RUN python3 -m pip install termcolor
RUN python3 -m pip install ujson
RUN python3 -m pip install GitPython
RUN python3 -m pip install tqdm
RUN python3 -m pip install scikit-learn
RUN python3 -m pip install psutil==5.9.8
RUN python3 -m pip install "torch>=1.14.0" "torchvision>=0.13.0" --index-url https://download.pytorch.org/whl/cpu
RUN python3 -m pip install "ultralytics==8.0.45"
RUN python3 -m pip install "pytorch_lightning==1.8.6"
RUN python3 -m pip install -U "git+https://github.com/lilohuang/PyTurboJPEG.git"
RUN python3 -m pip install flask waitress wtforms
RUN python3 -m pip install "gunicorn==21.2.0"

ARG USER=${USER}
ARG GROUP=${GROUP}
ARG USER_ID=${USER_ID}
ARG GROUP_ID=${GROUP_ID}

RUN addgroup --system --gid $GROUP_ID $GROUP && \
    adduser --system --uid $USER_ID --ingroup $GROUP $USER

RUN mkdir -p "/app/models" && mkdir "/app/pkg" && mkdir "/mpl"
WORKDIR "/app"
COPY . "/app"
RUN mkdir -p "/nonexistent/.cache/torch/hub/checkpoints/"
COPY "./models/models/pytorch_checkpoints" "/nonexistent/.cache/torch/hub/checkpoints/"

ENV PYTHONPATH "${PYTHONPATH}:/app/pkg"

RUN chown -R $USER:$GROUP /app /nonexistent /tmp /mpl

ENV MPLCONFIGDIR=/mpl

USER $USER
