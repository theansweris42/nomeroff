#!/bin/bash
set -a

DEBUG=0
DOWN=0
FORCE_RECREATE=""
BUILD=0
RUNSERVER=0
SHOWHELP=0
LOGS=0
NO_FLAGS=1

while [[ $# -gt 0 ]]; do
    case "$1" in
        -d|--debug)
            DEBUG=1
            ;;
        --down)
            DOWN=1
            NO_FLAGS=0
            ;;
        -f|--force)
            FORCE_RECREATE="--force-recreate"
            ;;
        -b|--build)
            BUILD=1
            NO_FLAGS=0
            ;;
        -r|--runserver)
            RUNSERVER=1
            NO_FLAGS=0
          ;;
        -l|--logs)
            LOGS=1
            NO_FLAGS=0
          ;;
        -h|--help)
            SHOWHELP=1
            ;;
        *)
            echo "Unknown option: $1, use --help to see the available options."
            exit 1
            ;;
    esac
    shift
done

if [ $SHOWHELP -eq 1 ] || [ $NO_FLAGS -eq 1 ]; then
    echo "Usage: ./runscript.sh [OPTIONS]"
    echo "Options:"
    echo "  -d, --debug          Run the server in debug mode."
    echo "  --down               Stop and remove the containers."
    echo "  -f, --force          Force recreate the containers."
    echo "  -b, --build          Build the containers."
    echo "  -r, --runserver      Run the backend server."
    echo "  -l, --logs           Show the logs of the running containers."
    echo "  -h, --help           Show this help message."
    exit 0
fi

if command -v git-lfs &>/dev/null; then
    echo "Checking if the Git LFS files are downloaded..."
    git lfs pull
else
    echo "Git LFS is not installed. Please install it before running this script."
    echo "You can install it by running the following command: sudo apt-get install git-lfs"
    exit 1
fi

echo "Sourcing the nomeroffrc file..."
source ./nomeroffrc


USER=$(whoami)
GROUP=$(whoami)
USER_ID=$(id -u)
GROUP_ID=$(id -g)

if [ -z "$USER" ]; then
  USER=nomeroffuser
fi

if [ -z "$GROUP" ]; then
  GROUP=nomeroffgroup
fi

if [ -z "$USER_ID" ]; then
  USER_ID=1000
fi

if [ -z "$GROUP_ID" ]; then
  GROUP_ID=1000
fi

export USER GROUP USER_ID GROUP_ID

if [ $DOWN -eq 1 ]; then
    echo "Stopping and removing the containers..."
    docker compose -f docker-compose.yml -p nomeroff down
    exit 0
fi

if [ $DEBUG -eq 1 ]; then
    # FIXME: hardcoded ports
    export NOMEROFF_NET_RUN_SRVR_COMMAND="flask run -h 0.0.0.0 -p 3116"
else
    CPU_COUNT=$(nproc)
    # FIXME: hardcoded ports
    export NOMEROFF_NET_RUN_SRVR_COMMAND="gunicorn --timeout 300 main:app --threads $CPU_COUNT --workers $CPU_COUNT --log-level info --bind \"0.0.0.0:3116\""
fi

if [ $LOGS -eq 1 ]; then
    echo "Showing the logs of the running containers..."
    docker compose -f docker-compose.yml -p nomeroff logs -f
    exit 0
fi

if [ $BUILD -eq 1 ]; then
    echo "Building the containers..."
    docker compose -f docker-compose.yml -p nomeroff build
fi

if [ $RUNSERVER -eq 1 ]; then
    echo "Running the nomeroff server..."
    docker compose -f docker-compose.yml -p nomeroff up $FORCE_RECREATE
fi

exit 0
