models_example = {
    "numberplate_options": {
        "url": "https://storage.yandexcloud.net/oinkoinkcar/models/options/torch/numberplate_options_2021_05_23.pt",
        "dataset": "",
        "repo": "",
        "commit_id": "4b4c7f677a205c7007d17aecca6f1f71942ae316",
        "description": "Numberplate Options Classification",
        "application": "Numberplate Options Application",
        "task": "Classification",
        "type": "Supervised Learning",
        "architecture": "CNN",
    }
}
